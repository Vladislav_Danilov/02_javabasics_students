package educationalCenter;

/**
 * This enum it contains constant learning outcomes
 * 
 * @author Vladislav_Danilov
 *
 */
public enum Result {
  Compleate(), NotCompleate(), BustTime(), MoreTime();
}
