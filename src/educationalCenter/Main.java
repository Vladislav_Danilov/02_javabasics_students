package educationalCenter;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {

  public static void main(String[] args) {

    // filling the list training J2ee
    List<Course> courseJ2eeList = new ArrayList<Course>();
    courseJ2eeList.add(new Course("���������� Java Servlets", 16));
    courseJ2eeList.add(new Course("Struts Framework", 24));
    courseJ2eeList.add(new Course("Spring Framework", 48));
    courseJ2eeList.add(new Course("Hibernate", 20));

    // filling the list curriculum J2ee
    Curriculum j2ee = new Curriculum("J2EE Developer", courseJ2eeList);

    // student data filling Ivanov Ivan
    Student ivanovIvan = new Student("Ivanov Ivan", Arrays.asList(2, 2, 2, 2, 2, 2),
        LocalDate.of(2014, Month.SEPTEMBER, 12), LocalDate.of(2014, Month.DECEMBER, 12), j2ee);

    List<Course> courseJavaDeveloper = new ArrayList<Course>();
    courseJavaDeveloper.add(new Course("����� ���������� Java", 8));
    courseJavaDeveloper.add(new Course("���������� JFC/Swing", 16));
    courseJavaDeveloper.add(new Course("���������� JDBC", 16));
    courseJavaDeveloper.add(new Course("���������� JAX", 52));
    courseJavaDeveloper.add(new Course("���������� commons", 44));

    Curriculum javaDeveloper = new Curriculum("Java Developer", courseJavaDeveloper);
    Student petrovPetr = new Student("Petrov Petr", Arrays.asList(4, 5, 3, 2, 3, 3, 5, 5),
        LocalDate.of(2014, Month.SEPTEMBER, 12), LocalDate.of(2014, Month.SEPTEMBER, 30),
        javaDeveloper);

    List<Course> courseCSharp = new ArrayList<Course>();
    courseCSharp.add(new Course("����� ���������� C#", 8));
    courseCSharp.add(new Course("����������", 16));
    courseCSharp.add(new Course("����������", 16));

    Curriculum cSharp = new Curriculum("C# Developer", courseJavaDeveloper);
    Student semenovSemen = new Student("Semenov Semen",
        Arrays.asList(3, 2, 3, 2, 3, 3, 2, 5, 2, 2, 3, 4, 5, 3, 2, 2),
        LocalDate.of(2014, Month.SEPTEMBER, 10), LocalDate.of(2014, Month.OCTOBER, 12), cSharp);

    List<Student> sortListStudentAverageRating = new ArrayList<Student>();
    // adding students in sorting list
    sortListStudentAverageRating.add(ivanovIvan);
    sortListStudentAverageRating.add(petrovPetr);
    sortListStudentAverageRating.add(semenovSemen);
    System.out.println("");
    System.out.println("���������� �� �������� �����");
    Collections.sort(sortListStudentAverageRating, (Student arg0, Student arg1) -> {
      return arg0.averageRating().compareTo(arg1.averageRating());
    });
    for (Student student : sortListStudentAverageRating) {
      student.outResult();
    }
    System.out.println("");
    System.out.println("���������� �� ������� �� ��������� ��������");
    Collections.sort(sortListStudentAverageRating, (Student arg0, Student arg1) -> {
      return arg0.takesTimeToRelease().compareTo(arg1.takesTimeToRelease());
    });

    for (Student student : sortListStudentAverageRating) {
      student.outResult();
    }
    System.out.println("");
    System.out.println("���� �����������, ��� �� ����� �������� =)");
    for (Student student : sortListStudentAverageRating) {
      if (student.analysisRatingFuture() >= 4.5) {
        student.outResult();
      }
    }
  }

}
