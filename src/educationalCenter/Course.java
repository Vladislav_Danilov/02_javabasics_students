package educationalCenter;

/**
 * 
 * This class defines a training session and its duration
 * 
 * @author Vladislav_Danilov
 *
 */
public class Course {
  private String nameCorse;
  private int duration;


  public String getNameCorse() {
    return nameCorse;
  }


  public void setNameCorse(String nameCorse) {
    this.nameCorse = nameCorse;
  }


  public int getDuration() {
    return duration;
  }


  public void setDuration(int duration) {
    this.duration = duration;
  }


  /**
   * The class constructor
   * 
   * @param nameCorse the name of the training session
   * @param duration duration in hours
   */
  public Course(String nameCorse, int duration) {
    this.nameCorse = nameCorse;
    this.duration = duration;
  }
}
