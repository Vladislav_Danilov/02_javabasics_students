package educationalCenter;

import java.util.List;

/**
 * This class defines the curriculum
 * 
 * @author Vladislav_Danilov
 *
 */
public class Curriculum {
  private List<Course> courseList;
  private String nameCurriculum;

  public List<Course> getCourseList() {
    return courseList;
  }

  public void setCourseList(List<Course> courseList) {
    this.courseList = courseList;
  }

  public String getNameCurriculum() {
    return nameCurriculum;
  }

  public void setNameCurriculum(String nameCurriculum) {
    this.nameCurriculum = nameCurriculum;
  }

  /**
   * The class constructor
   * 
   * @param nameCurriculum the name of the curriculum
   * @param courseList a list of training sessions
   */
  public Curriculum(String nameCurriculum, List<Course> courseList) {
    this.courseList = courseList;
    this.nameCurriculum = nameCurriculum;
  }
}
