package educationalCenter;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

/**
 * This class defines the essence of the student and the operations with its data
 * 
 * @author Vladislav_Danilov
 *
 */
public class Student {
  String studentName;
  List<Integer> marksList;
  LocalDate startDateFirstCourse;
  LocalDate startDateTask;
  Curriculum curriculum;

  /**
   * The class constructor
   * 
   * @param studentName student's name
   * @param marksList list of ratings
   * @param startDateFirstCourse the beginning of the training program
   * @param startDateTask date of receipt of the job
   * @param curriculum data on the curriculum
   */
  public Student(String studentName, List<Integer> marksList, LocalDate startDateFirstCourse,
      LocalDate startDateTask, Curriculum curriculum) {
    super();
    this.studentName = studentName;
    this.marksList = marksList;
    this.startDateFirstCourse = startDateFirstCourse;
    this.startDateTask = startDateTask;
    this.curriculum = curriculum;
  }


  /**
   * It calculates the number of hours before graduation
   * 
   * @return number of hours
   */
  public Integer takesTimeToRelease() {
    Long periodStudy = ChronoUnit.DAYS.between(startDateFirstCourse, startDateTask);
    Integer fullTimeStudy = 0;
    for (Course course : curriculum.getCourseList()) {
      fullTimeStudy = fullTimeStudy + course.getDuration();
    }
    return (int) (periodStudy * 8 - fullTimeStudy);
  }

  /**
   * calculates the average rating
   * 
   * @return average rating
   */
  public Float averageRating() {
    int allRating = 0;
    for (Integer rating : marksList) {
      allRating = allRating + rating;
    }
    if (marksList.size() > 0) {
      return ((float) allRating / marksList.size());
    } else {
      return 0.0f;
    }
  }

  /**
   * It analyzes whether the student can continue training
   * 
   * @return possible evaluation of the future
   */
  public Float analysisRatingFuture() {
    int takesCurseToRelease = takesTimeToRelease() / 8;
    int allRating = 0;
    for (Integer rating : marksList) {
      allRating = allRating + rating;
    }
    return ((float) (allRating + 5 * takesCurseToRelease))
        / (marksList.size() + takesCurseToRelease);
  }


  /**
   * computes the result of learning
   * 
   * @return learning outcome
   */
  public Result takesResult() {
    if (analysisRatingFuture() >= 4.5) {
      return Result.MoreTime;
    } else if (analysisRatingFuture() < 4.5) {
      return Result.NotCompleate;
    } else if (analysisRatingFuture() >= 4.5 && takesTimeToRelease() < 0) {
      return Result.Compleate;
    } else {
      return Result.BustTime;
    }
  }

  /**
   * outputs the result to the console
   * 
   * @return end method
   */
  public boolean outResult() {
    DecimalFormat df = new DecimalFormat("#.#");
    df.setRoundingMode(RoundingMode.CEILING);
    StringBuilder out = new StringBuilder();
    String result = "";
    switch (takesResult()) {
      case MoreTime:
        result = ". ����� ���������� ��������.";
        break;
      case NotCompleate:
        result = ". ���������.";
        break;
      case Compleate:
        result = ". ������� �������� ��������.";
        break;
      default:
        break;
    }
    System.out.println(out.append(studentName).append(" - �� ��������� �������� �� ��������� ")
        .append(curriculum.getNameCurriculum()).append(" �������� ").append(takesTimeToRelease())
        .append(" �. ������� ���� ").append(df.format(averageRating())).append(result));
    return true;
  }
}
